import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { CadImovelModule } from './components/cad-imovel/cad-imovel.module';
import { HomeModule } from './components/home/home.module';
import { ImoveisRuraisModule } from './components/imoveis-rurais/imoveis-rurais.module';
import { ImoveisUrbanosModule } from './components/imoveis-urbanos/imoveis-urbanos.module';
import { FooterModule } from './components/footer/footer.module';
import { NavbarModule } from './components/navbar/navbar.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HomeModule,
    CadImovelModule,
    ImoveisRuraisModule,
    ImoveisUrbanosModule,
    NavbarModule,
    FooterModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
