import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ImoveisRuraisComponent } from './imoveis-rurais.component';
import { DestaquesComponent } from './destaques/destaques.component';



@NgModule({
  declarations: [
    ImoveisRuraisComponent,
    DestaquesComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    ImoveisRuraisComponent
  ]
})
export class ImoveisRuraisModule { }
