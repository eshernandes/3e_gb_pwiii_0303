import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-anuncios-dicas',
  templateUrl: './anuncios-dicas.component.html',
  styleUrls: ['./anuncios-dicas.component.css']
})
export class AnunciosDicasComponent implements OnInit {

  anuncios = [
    {
      id: 0,
      image: '../../../../assets/images/saopaulo.webp',
      cidade: 'São Paulo'
    },
    {
      id: 1,
      image: '../../../../assets/images/rio.webp',
      cidade: 'Rio de Janeiro'
    },
    {
      id: 2,
      image: '../../../../assets/images/bh.webp',
      cidade: 'Belo Horizonte'
    },
    {
      id: 3,
      image: '../../../../assets/images/portoalegre.jpg',
      cidade: 'Porto Alegre'
    }
  ]

  dicas = [
    {
      id: 0,
      image: '../../../../assets/images/comprar.webp',
      title: 'O que preciso para comprar?',
      button: 'Guia de compra'
    },
    {
      id: 1,
      image: '../../../../assets/images/alugar.webp',
      title: 'O que preciso para alugar?',
      button: 'Guia de aluguel'
    },
    {
      id: 2,
      image: '../../../../assets/images/vender.webp',
      title: 'O que preciso para vender?',
      button: 'Guia de venda'
    },
  ]
  constructor() { }

  ngOnInit(): void {
  }

}
