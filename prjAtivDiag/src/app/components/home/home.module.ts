import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home.component';
import { DestaquesComponent } from './destaques/destaques.component';
import { FiltragemComponent } from './filtragem/filtragem.component';
import { AnunciosDicasComponent } from './anuncios-dicas/anuncios-dicas.component';



@NgModule({
  declarations: [
    HomeComponent,
    DestaquesComponent,
    FiltragemComponent,
    AnunciosDicasComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    HomeComponent
  ]
})
export class HomeModule { }
