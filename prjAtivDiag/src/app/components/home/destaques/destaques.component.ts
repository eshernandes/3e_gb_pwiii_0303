import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-destaques',
  templateUrl: './destaques.component.html',
  styleUrls: ['./destaques.component.css']
})
export class DestaquesComponent implements OnInit {

  color = ''
  destaques = [
    {
      id: 0,
      imagem: '../../../../assets/images/saosebastiao.jpg',
      cidade: 'São Sebastião',
      distancia: '128km de distância'
    },
    {
      id: 1,
      imagem: '../../../../assets/images/guaruja.jpg',
      cidade: 'Guarujá',
      distancia: '64km de distância'
    },
    {
      id: 2,
      imagem: '../../../../assets/images/praiagrande.webp',
      cidade: 'Praia Grande',
      distancia: '56km de distância'
    },
    {
      id: 3,
      imagem: '../../../../assets/images/ubatuba.jpg',
      cidade: 'Ubatuba',
      distancia: '157km de distância'
    }
  ]
  constructor() { }

  ngOnInit(): void {
  }

  getColor(i: number){
    switch(i){
      case 0:
        return '#800080'
      case 1:
        return '#9F2B68'
      case 2:
        return '#7F00FF'
      case 3:
        return '#BF40BF'
      default:
        return ''
    }
  }
}
