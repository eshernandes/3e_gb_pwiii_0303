import { ImoveisRuraisComponent } from './components/imoveis-rurais/imoveis-rurais.component';
import { CadImovelComponent } from './components/cad-imovel/cad-imovel.component';
import { HomeComponent } from './components/home/home.component';

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', component: HomeComponent},
  { path: 'addimovel', component: CadImovelComponent},
  { path: 'rurais', component: ImoveisRuraisComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
